{{- define "app.labels" -}}
app: {{ include "app.name" . }}
deployment: {{ include "app.name" . }}-deployment
{{- end -}}

{{- define "app.selectorLabels" -}}
app: {{ include "app.name" . }}
deployment: {{ include "app.name" . }}-deployment
{{- end -}}