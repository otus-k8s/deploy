{{- define "app.name" -}}
{{- default .Chart.Name | trunc 63 | trimSuffix "-" -}}
{{- end -}}

{{- define "app.image.go" -}}
  {{- $registry := .Values.svc.image.registry -}}
  {{- $image    := .Values.svc.image.appImage -}}
  {{- $registry.domain }}/{{ $registry.repo }}/{{ $image.name }}:{{ $image.tag }}
{{- end -}}

{{- define "app.healthcheck" -}}
{{- range $values := . }}
  {{ $values.name }}:
    failureThreshold: {{ $values.thresholds.failure }}
    {{- if eq $values.type "httpGet" }}
    httpGet:
      path: {{ $values.url }}
      port: {{ $values.port }}
      scheme: {{ default "http" $values.protocol | upper  }}
    {{- else if eq $values.type "exec" }}
    exec:
      command: {{ $values.command }}
    {{- end }}
    initialDelaySeconds: {{ $values.thresholds.initial }}
    periodSeconds: {{ $values.thresholds.period }}
    successThreshold: {{ $values.thresholds.success }}
    timeoutSeconds: {{ $values.thresholds.timeout }}
{{- end -}}
{{- end -}}


{{- define "app.env" -}}
{{ range $name, $value := . -}}
- name: {{ $name | upper }}
  value: {{ $value | quote }}
{{ end }}
{{- end -}}
