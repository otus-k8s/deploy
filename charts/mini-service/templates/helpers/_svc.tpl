{{- define "app.svc" -}}
  {{- range $values := . }}
- name: {{ $values.name }}
  targetPort: {{ $values.name }}
  protocol: {{ default "tcp" $values.proto | upper  }}
  port: {{ required ( printf "Service port is not set for %s" $values.name ) $values.port }}
  {{- end }}
{{- end -}}

{{- define "app.ports" -}}
  {{- range $values := . }}
- name: {{ $values.name }}
  containerPort: {{ $values.port }}
  protocol: {{ default "tcp" $values.proto | upper  }}
  {{- end }}
{{- end -}}
