{{- define "go.containers" -}}
- name: {{ include "app.name" . }}-go
  image: {{ include "app.image.go" $ }}
  securityContext:
    runAsUser: 1000
    runAsGroup: 1000
  lifecycle:
    preStop:
      exec:
        command: ["/bin/sh","-c","/bin/sleep 10; /bin/kill -QUIT 1"]
  env:
  {{- include "app.env" $.Values.svc.go.env | nindent 2 }}
  {{- include "app.env" $.Values.svc.env | nindent 2 }}
  {{- if .Values.svc.image.pullPolicy }}
  imagePullPolicy: {{ .Values.svc.image.pullPolicy }}
  {{- end }}
  ports: {{- include "app.ports" .Values.svc.go.ports | indent 2 }}
  resources:
{{ toYaml .Values.svc.go.resources | indent 4 }}
  {{ include "app.healthcheck" .Values.svc.go.healthcheck | nindent 0 }}
  {{- with .Values.svc.go.extraVolumeMounts }}
  volumeMounts:
    {{- toYaml . | nindent 4 }}
  {{- end }}
{{- end -}}
